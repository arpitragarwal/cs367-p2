///////////////////////////////////////////////////////////////////////////////
// Main Class File:  Receiver.java
// File: 			 PacketLinkedListIterator.java
// Semester:         (CS 367) Spring 2016
//
// Author(s):        (Tone Yu, Arpit Agarwal)
// Email:            (tyu44@wisc.edu)
// CS Login:         (tone)
// Lecturer's Name:  (Skrenty)
//
////////////////////////////////////////////////////////////////////////////////
//
// Pair Partner:     (Arpit Agarwal)
// Email:            (agarwal32@wisc.edu)
// CS Login:         (arpit)
// Lecturer's Name:  (Skrenty)
////////////////////////////////////////////////////////////////////////////////
import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * The iterator implementation for PacketLinkedList.
 */
public class PacketLinkedListIterator<T> implements Iterator<T> {
	// TODO: add field when needed
	Listnode<T> curr = null;
	/**
	 * Constructs a PacketLinkedListIterator by passing a head node of
	 * PacketLinkedList.
	 * 
	 * @param head
	 */
	public PacketLinkedListIterator(Listnode<T> head) {
		// TODO
		curr = head.getNext();
	}
	
	/**
	 * Returns the next element in the iteration.
	 * @return the next element in the iteration
	 * @throws NoSuchElementException if the iteration has no more elements
	 */
	@Override
	public T next() {
		//TODO: replace the default return statment
		T item = curr.getData();
		curr = curr.getNext();
		return item; 
	}
	
	/**
	 * Returns true if the iteration has more elements.
	 * @return true if the iteration has more elements
	 */
	@Override
	public boolean hasNext() {
		//TODO: replace the default return statment
		//if (curr.getNext()!=null) return true;
		//else return false;
		return curr!=null;
	}

        /**
         * The remove operation is not supported by this iterator
         * @throws UnsupportedOperationException if the remove operation is not supported by this iterator
         */
        @Override
	public void remove() {
	  throw new UnsupportedOperationException();
	}
}
