///////////////////////////////////////////////////////////////////////////////
// Main Class File:  Receiver.java
// File: 			 BadImageContentException.java
// Semester:         (CS 367) Spring 2016
//
// Author(s):        (Tone Yu, Arpit Agarwal)
// Email:            (tyu44@wisc.edu)
// CS Login:         (tone)
// Lecturer's Name:  (Skrenty)
//
////////////////////////////////////////////////////////////////////////////////
//
// Pair Partner:     (Arpit Agarwal)
// Email:            (agarwal32@wisc.edu)
// CS Login:         (arpit)
// Lecturer's Name:  (Skrenty)
////////////////////////////////////////////////////////////////////////////////
/**
 * This exception should be thrown whenever the received image is 
 * broken due to a corrupt content.
 * 
 */
public class BadImageContentException extends RuntimeException {

	/**
	 * Constructs a BadImageContentException with a message
	 * @param s the error message
	 */
	public BadImageContentException(String s) {
		super(s);
	}

	/**
	 * Constructs a BadImageContentException
	 */
	public BadImageContentException() {
		super();
	}
}