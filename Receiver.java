///////////////////////////////////////////////////////////////////////////////
// Title:            (Program 2)
// Files:            (BadImageContentException.java, BadImageHeaderException.java,
//					 BrokenImageException.java, ImageDriver.java, InputDriver.java,
//					 ListADT.java, Listnode.java, PacketLinkedList.java,
//					 PacketLinkedListIterator.java, Receiver.java, SimplePacket.java)
// Semester:         (CS 367) Spring 2016
//
// Author:           (Tone Yu, Arpit Agarwal)
// Email:            (tyu44@wisc.edu)
// CS Login:         (tone)
// Lecturer's Name:  (Skrenty)
//
////////////////////////////////////////////////////////////////////////////////
//
// Pair Partner:     (Arpit Agarwal)
// Email:            (agarwal32@wisc.edu)
// CS Login:         (arpit)
// Lecturer's Name:  (Skrenty)
////////////////////////////////////////////////////////////////////////////////
import java.io.IOException;

/**
 * The main class. It simulates a application (image viewer) receiver by
 * maintaining a list buffer. It collects packets from the queue of 
 * InputDriver and arrange them properly, and then reconstructs the image
 * file from its list buffer.
 */
public class Receiver {
	private InputDriver input;
	private ImageDriver img;
	private PacketLinkedList<SimplePacket> list;
	private int sizeOfStream = 0;

	/**
	 * Constructs a Receiver to obtain the image file transmitted.
	 * @param file the filename you want to receive
	 */
	public Receiver(String file) {
		try {
			input = new InputDriver(file, true);
		} catch (IOException e) {
			System.out.println(
					"The file, " + file + ", isn't existed on the server.");
			System.exit(0);
		}
		img = new ImageDriver(input);
		// TODO: properly initialize your field
		list = new PacketLinkedList<SimplePacket>();
	}

	/**
	 * Returns the PacketLinkedList buffer in the receiver
	 * 
	 * @return the PacketLinkedList object
	 */
	public PacketLinkedList<SimplePacket> getListBuffer() {
		return list;
	}
	
	/**
	 * Asks for retransmitting the packet. The new packet with the sequence 
	 * number will arrive later by using {@link #askForNextPacket()}.
	 * Notice that ONLY packet with invalid checksum will be retransmitted.
	 * 
	 * @param pkt the packet with bad checksum
	 * @return true if the requested packet is added in the receiving queue; otherwise, false
	 */
	public boolean askForRetransmit(SimplePacket pkt) {
		return input.resendPacket(pkt);
	}

        
	/**
	 * Asks for retransmitting the packet with a sequence number. 
	 * The requested packet will arrive later by using 
	 * {@link #askForNextPacket()}. Notice that ONLY missing
	 * packet will be retransmitted. Pass seq=0 if the missing packet is the
	 * "End of Streaming Notification" packet.
	 * 
	 * @param seq the sequence number of the requested missing packet
	 * @return true if the requested packet is added in the receiving queue; otherwise, false
	 */
	public boolean askForMissingPacket(int seq) {
		return input.resendMissingPacket(seq);
	}

	/**
	 * Returns the next packet.
	 * 
	 * @return the next SimplePacket object; returns null if no more packet to
	 *         receive
	 */
	public SimplePacket askForNextPacket() {
		return input.getNextPacket();
	}

	/**
	 * Returns true if the maintained list buffer has a valid image content. Notice
	 * that when it returns false, the image buffer could either has a bad
	 * header, or just bad body, or both.
	 * 
	 * @return true if the maintained list buffer has a valid image content;
	 *         otherwise, false
	 */
	public boolean validImageContent() {
		return input.validFile(list);
	}

	/**
	 * Returns if the maintained list buffer has a valid image header
	 * 
	 * @return true if the maintained list buffer has a valid image header;
	 *         otherwise, false
	 */
	public boolean validImageHeader() {
		return input.validHeader(list.get(0));
	}
	
	/**
	 * Outputs the formatted content in the PacketLinkedList buffer. See
	 * course webpage for the formatting detail.
	 */
	public void displayList() {
		// TODO: implement this method firstly to help you debug
		System.out.println("");
		//System.out.print("here's the final list: ");
		PacketLinkedListIterator<SimplePacket> itr = list.iterator();
		SimplePacket curr = null;
		while(itr.hasNext()){
			curr = itr.next();
			if(curr.isValidCheckSum()){
				System.out.print(curr.getSeq());
				System.out.print(", ");
			} else {
				System.out.print("[");
				System.out.print(curr.getSeq());
				System.out.print("], ");	
			}
		}
	//System.out.print(list.get(0).getSeq());
	}

	private void addPackets() {
		boolean isNotEnd = true;
        while(isNotEnd){
        	SimplePacket newpacket = this.askForNextPacket();
        	if(newpacket==null) break;   	
        	PacketLinkedListIterator<SimplePacket> itr = list.iterator();
        	int counter = 0;
        	boolean isduplicate = false;
        	while(itr.hasNext() && !isduplicate){
        		if(itr.next().getSeq()==newpacket.getSeq()){
           			isduplicate = true;
       			}
       			counter++;
        	}
            if (isduplicate){
       	    	list.remove(counter);
       	    	if (newpacket.isValidCheckSum()){
        	   		if(newpacket.getSeq()>0){
        	   			list.add(counter, newpacket);
        	   		} else {
        	   			sizeOfStream = -1*newpacket.getSeq();
            		}
       	    	}else{
       	    		this.askForRetransmit(newpacket);
       	    	}
       	    } else {
       	    	if (newpacket.isValidCheckSum()){
       	    		if(newpacket.getSeq()>0){
       	    			PacketLinkedListIterator<SimplePacket> iter = list.iterator();
        	   			SimplePacket curr = null;
        	   			int seq = 0;
        	   			boolean added = false;
        	   			while(iter.hasNext() && !added){
            				curr = iter.next();
       	    				seq++;
       	    				if(newpacket.getSeq()<curr.getSeq()){
       	    					list.add(seq, newpacket);
       	    					added = true;
       	    				}
       	    			}
       	    			if(!added) list.add(newpacket);
       	    		}else{
       	    			sizeOfStream = -1*newpacket.getSeq();
        	    	}
        	    } else {
        	   		this.askForRetransmit(newpacket);
        	   	}
        	}	
        }
	}
	
	/**
	 * Reconstructs the file by arranging the {@link PacketLinkedList} in
	 * correct order. It uses {@link #askForNextPacket()} to get packets until
	 * no more packet to receive. It eliminates the duplicate packets and asks
	 * for retransmitting when getting a packet with invalid checksum.
	 */
	public void reconstructFile() {
		// TODO: Collect the packets and arrange them in order.
		// 		 You can try to collect all packets and put them into your list
		//       without any processing and use implemented displayList() to see
		//       the pattern of packets you are going to receive.
		//       Then, properly handle the invalid checksum and duplicates. The 
		//       first image file, "secret0.jpg", would not result in missing
		//       packets into your receiving queue such that you can test it once
		//       you get the first two processing done.
	
		//
		
		// TODO: Processing missing packets for the other four images. You should
		//       utilize the information provided by "End of Streaming Notification
		//       Packet" though this special packet could be lost while transmitting.

		//
		this.addPackets();
        
        //asking for missing packets
		if(sizeOfStream==0) this.askForMissingPacket(0);
		this.addPackets();
        for(int i=1; i<=sizeOfStream; i++){
        	this.askForMissingPacket(i);
        }
        this.addPackets();   
    }

	/**
	 * Opens the image file by merging the content in the maintained list
	 * buffer.
	 */
	public void openImage() {	
		try {
			img.openImage(list);
			
		} 
		// TODO: catch the image-related exception
		/* throws BadImageHeaderException if the maintained list buffer has an 
		 * invalid image header, throws BadImageContentException if the 
		 * maintained list buffer has an invalid image content*/
		catch(BrokenImageException e){
			
			if(!validImageHeader()){
				throw new BadImageHeaderException("The image is broken due to a damaged header");
			}
			if(!validImageContent()){
			throw new BadImageContentException("The image is broken due to a corrupt content");
			}
		}
		
		catch (Exception e) {
			System.out.println(
					"Please catch the proper Image-related Exception.");
			e.printStackTrace();
		}
	}

	/**
	 * Initiates a Receiver to reconstruct collected packets and open the Image
	 * file, which is specified by args[0]. 
	 * 
	 * @param args command line arguments
	 */
	public static void main(String[] args) {
	        if (args.length != 1) {
                  System.out.println("Usage: java Receiver [filename_on_server]");
                  return;
	        }
		Receiver recv = new Receiver(args[0]);
		recv.reconstructFile();
        recv.displayList(); //use for debugging
		recv.openImage();
	}
}