///////////////////////////////////////////////////////////////////////////////
// Main Class File:  Receiver.java
// File: 			 PacketLinkedList.java
// Semester:         (CS 367) Spring 2016
//
// Author(s):        (Tone Yu, Arpit Agarwal)
// Email:            (tyu44@wisc.edu)
// CS Login:         (tone)
// Lecturer's Name:  (Skrenty)
//
////////////////////////////////////////////////////////////////////////////////
//
// Pair Partner:     (Arpit Agarwal)
// Email:            (agarwal32@wisc.edu)
// CS Login:         (arpit)
// Lecturer's Name:  (Skrenty)
////////////////////////////////////////////////////////////////////////////////
/**
 * A Single-linked linkedlist with a "dumb" header node (no data in the node),
 * but without a tail node. It implements ListADT<E> and returns
 * PacketLinkedListIterator when requiring a iterator.
 */
public class PacketLinkedList<E> implements ListADT<E> {
	// TODO: Add your fields here.
	private Listnode<E> head;
	private int numItems;

	// Please see ListADT for detailed javadoc
	//

	/**
	 * Constructs a empty PacketLinkedList
	 */
	public PacketLinkedList() {
		// TODO
		head = new Listnode<E>(null);
		numItems = 0;
	}

	@Override
	public void add(E item) {
		// TODO
		if (item == null) {
			throw new IllegalArgumentException();
		}
		Listnode<E> newnode = new Listnode<E>(item);
		Listnode<E> curr = head;
		while (curr.getNext() != null) {
			curr = curr.getNext();
		}
		curr.setNext(newnode);
		numItems++;
	}

	@Override
	public void add(int pos, E item) {
		// TODO
		Listnode<E> newnode = new Listnode<E>(item);
		Listnode<E> curr = head;
		for (int i = 0; i < pos-1; i++) {
			curr = curr.getNext();
		}
		newnode.setNext(curr.getNext());
		curr.setNext(newnode);
	}

	@Override
	public boolean contains(E item) {
		// TODO: replace the default return statement
		boolean exists = false;
		Listnode<E> curr = head;

		while (curr.getNext() != null) {
			if (curr.getData().equals(item)) {
				exists = true;
			}
		}
		return exists;
	}

	@Override
	public E get(int pos) {
		// TODO: replace the default return statement
		// if(pos < 0 || pos >= numItems){
		// throw new IndexOutofBoundsException();
		Listnode<E> curr = head.getNext();
		for (int i = 0; i < pos; i++) {
			curr = curr.getNext();
		}
		return curr.getData();
	}

	@Override
	public boolean isEmpty() {
		// TODO: replace the default return statement
		if (numItems == 0) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public E remove(int pos) {
		// TODO: replace the default return statement
		Listnode<E> curr = head;
		Listnode<E> newnode = new Listnode<E>(null);
		for (int i = 0; i < pos - 1; i++) {
			curr = curr.getNext();
		}
		newnode = curr.getNext();
		curr.setNext(curr.getNext().getNext());
		return newnode.getData();
	}

	@Override
	public int size() {
		// TODO: replace the default return statement
		return numItems;
	}

	@Override
	public PacketLinkedListIterator<E> iterator() {
		// TODO: replace the default return statement
		return new PacketLinkedListIterator(head);
	}

}