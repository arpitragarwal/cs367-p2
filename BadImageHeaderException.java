///////////////////////////////////////////////////////////////////////////////
// Main Class File:  Receiver.java
// File: 			 BadImageHeaderException.java
// Semester:         (CS 367) Spring 2016
//
// Author(s):        (Tone Yu, Arpit Agarwal)
// Email:            (tyu44@wisc.edu)
// CS Login:         (tone)
// Lecturer's Name:  (Skrenty)
//
////////////////////////////////////////////////////////////////////////////////
//
// Pair Partner:     (Arpit Agarwal)
// Email:            (agarwal32@wisc.edu)
// CS Login:         (arpit)
// Lecturer's Name:  (Skrenty)
////////////////////////////////////////////////////////////////////////////////
/**
 * This exception should be thrown whenever the received image is 
 * broken due to a damaged header.
 * 
 */
public class BadImageHeaderException extends RuntimeException {

	/**
	 * Constructs a BadImageHeaderException with a message
	 * @param s the error message
	 */
	public BadImageHeaderException(String s) {
		super(s);
	}

	/**
	 * Constructs a BadImageHeaderException
	 */
	public BadImageHeaderException() {
		super();
	}
}
